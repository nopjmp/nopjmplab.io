# tilde.moe

My Hugo powered blog. Thanks to [@bovarysme](https://github.com/bovarysme) for the hugo base!

## Features

- an Atom feed
- permalinks similar to Jekyll's defaults
- a theme heavily based on [poole/poole](https://github.com/poole/poole)

## License

This project is licensed under the terms of the MIT license. Theme license is in THEME_LICENSE.md
