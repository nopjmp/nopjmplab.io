---
title: "Ransomware Fail"
date: 2018-11-26T7:59:13-05:00
categories: ["ransomware", "fail", "dotnet"]
---

We were alerted this morning about one of our smaller clients getting a ransomware dialog on their server. It took us a while to free up resources for us to respond, but we got there for our initial take on the situation. Skull and "digital" styled background with an amount of bitcoin needed in order to decrypt the files. I looked around the drive for any signs of hope, but nothing jumped out at me. The only machine touched so far was their primary server which was a major relief. We decided to take a lunch and return in addition to me getting my laptop from our office.

After lunch, I returned with my laptop in hand and looked more thoroughly and noticed a few things. The file extensions when search on Google, gave me a result for the source code to this specific piece of kit. This kit includes many modules ranging from keyloggers to ransomware. Curious, I started going through the ransomware related modules to find out if it was possible for me to decrypt the files.

I noticed a few things:
* The keys were stored on the server.
* The protocol was encrypted.
* The settings for the encryption were in a configuration file that was compiled in.
* The software used a .NET language.

I used [dotPeek] to investigate into the matter further. It was then I realized they attempted to obfuscate the compiled code and it came up in seemingly random characters. Curious, I poked around until I found a file that resembled the settings configuration file I mentioned earlier. I used the "Find Usages" feature to find context clues on what each individual item was in the original configuration file and populated them. This is where I realized something...

The software had a "decryption" feature that could be send by the server to start the decryption process... **AND IT REQUESTED THE KEY WITHOUT ANY KIND OF AUTHENTICATION**

My thinking cap came on and I started to see if it was viable for me to create a solution. I figured it would take their circuit 2 days to fully restore their data going full speed, and it would take me less than a few hours to decrypt their data.

At this point, I started to clobber together a new program using parts of the software and set everything up as expected. I figured out the servers IP address and port, setup the decryption function, added a little bit of output and fired the program off.

It did take me a few times to remember how to program in this language so my code wouldn't crash, but overall I think it pays to be a little mindful of programming and to be ready for when your adversary does the most idiotic thing imaginable... **Using open-source malware**

TL;DR a script kiddie attack one of my clients and I was able to get all of their data back.

**NOTE:** I will not be giving out details for a bit to avoid the ransomware authors may find this blog and patch their code.

[dotPeek]: https://www.jetbrains.com/decompiler/