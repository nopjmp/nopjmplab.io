---
title: "About"
date: 2018-09-01T21:44:19-05:00
menu: "main"
---

Just an adventure in the realms of the digital landscape.

I own too many domain names and my favorite color is purple.

I spend my time looking at funny memes and dissecting the world around me.

### Contact me

[jmp@0x.gg](mailto:jmp@0x.gg)
